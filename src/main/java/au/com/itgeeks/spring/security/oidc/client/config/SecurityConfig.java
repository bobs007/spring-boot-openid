package au.com.itgeeks.spring.security.oidc.client.config;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeAccessTokenProvider;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import au.com.itgeeks.spring.security.oidc.client.filter.OIDCConnectFilter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private OAuth2RestTemplate restTemplate;

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/resources/**");
  }

  @Bean
  public OIDCConnectFilter myFilter() throws Exception {
    final OIDCConnectFilter filter = new OIDCConnectFilter("/google-login");
    filter.setRestTemplate(restTemplate);
    disableCertificateChecks(restTemplate);
    return filter;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().addFilterAfter(new OAuth2ClientContextFilter(),
        AbstractPreAuthenticatedProcessingFilter.class)
        .addFilterAfter(myFilter(), OAuth2ClientContextFilter.class).httpBasic()
        .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/google-login")).and()
        .authorizeRequests().anyRequest().authenticated();
  }
  
  
  private void disableCertificateChecks(OAuth2RestTemplate oauthTemplate) throws Exception {

    SSLContext sslContext = SSLContext.getInstance("TLS");
    sslContext.init(null, new TrustManager[] { new Dumb509TrustManager() }, null);
    ClientHttpRequestFactory requestFactory = new SSLContextRequestFactory(sslContext);

    // This is for OAuth protected resources
    oauthTemplate.setRequestFactory(requestFactory);

    // AuthorizationCodeAccessTokenProvider creates it's own RestTemplate for token operations
    AuthorizationCodeAccessTokenProvider provider = new AuthorizationCodeAccessTokenProvider();
    provider.setRequestFactory(requestFactory);
    oauthTemplate.setAccessTokenProvider(provider);
}


}
