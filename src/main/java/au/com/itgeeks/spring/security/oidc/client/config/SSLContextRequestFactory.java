package au.com.itgeeks.spring.security.oidc.client.config;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.springframework.http.client.SimpleClientHttpRequestFactory;

public class SSLContextRequestFactory extends SimpleClientHttpRequestFactory {

  private final SSLContext sslContext;

  public SSLContextRequestFactory(final SSLContext sslContext) {
    this.sslContext = sslContext;
  }

  @Override
  protected void prepareConnection(final HttpURLConnection connection, final String httpMethod)
      throws IOException {
    if (connection instanceof HttpsURLConnection) {
      ((HttpsURLConnection) connection).setSSLSocketFactory(sslContext.getSocketFactory());
    }
    super.prepareConnection(connection, httpMethod);
  }

}
