package au.com.itgeeks.spring.security.oidc.client.config;

import java.security.cert.CertificateException;

import javax.net.ssl.X509TrustManager;

public class Dumb509TrustManager implements X509TrustManager {

  @Override
  public void checkClientTrusted(final java.security.cert.X509Certificate[] chain,
      final String authType) throws CertificateException {

  }

  @Override
  public void checkServerTrusted(final java.security.cert.X509Certificate[] chain,
      final String authType) throws CertificateException {

  }

  @Override
  public java.security.cert.X509Certificate[] getAcceptedIssuers() {
    return null;
  }

}
